n = None
b = True
i = 12
d = 12.1
f = "man"


def min(n,m):
    if n < m:
        return n
    else:
        return m


def max(n,m):
    if n > m:
        return n
    else:
        return m

def createListe(n,m):
    liste = []
    for e in range(min(n,m), max(n,m) + 1):
        liste.append(e)
    return liste

def funct(liste):
    erg = []
    for e in liste:
        k = 2*(e*e) + 4*e - 5
        erg.append(k)
    return erg

n = int(input("n: "))
m = int(input("m: "))
print(funct(createListe(n,m)))