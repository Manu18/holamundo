def prim(n):
    sieb = []
    for i in range(2,n+1):
        sieb.append(i)
    i = 0
    p = sieb[i]
    while p*p <= n:
        v = 2*p
        while v <= sieb[-1]:
            if v in sieb:
                sieb.remove(v)
            v = v+p
        i = i+1
        p = sieb[i]
    return [1] + sieb


def istPrim(i):
    L = prim(i)
    for e in L:
        if e == i:
            return True
    return False



def sumPrim():
    n = int(input("Gebe n ein: "))
    sum = 0
    L = prim(n)
    for e in L:
        sum += e
    print("Die Summe aller Primzahlen bis ", n ," ist ", sum)

if __name__ == "__main__":
    print(prim(10))
    print(istPrim(7))
    sumPrim()

