class BElement:
    def __init__(self, i, v):
        self.__id = i
        self.__value = v
        self.__left = None
        self.__right = None

    def id(self):
        return self.__id

    def value(self):
        return self.__value

    def getLeft(self):
        return self.__left

    def setLeft(self, l):
        self.__left = l

    def getRight(self):
        return self.__right

    def setRight(self, r):
        self.__right = r

    def __str__(self):
        s = "{"
        if None != self.__left :
            s = s + str(self.__left)
        s = s + str(self.__id) + ":" + str(self.__value)
        if None != self.__right:
            s = s + str (self.__right)
        s = s + "}"
        return s


class BListeAlsEVL:
    def __init__(self):
        self.__head = None

    def insert(self, i, v):
        e = BElement(i ,v)
        if None == self.__head:
            self.__head = e
        else:
            self.__insert(self.__head, e)

    def __insert(self, h, e):
        # your code here
        pass






    def __find(self, element, i):
        # your code here
        pass







    def find(self, i):
        if None == self.__head :
            return -1
        else:
            return self.__find(self.__head, i)

    def __str__(self):
        if None == self.__head:
            return "{}"
        else:
            return str( self.__head)