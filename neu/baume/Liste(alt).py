import random
import time
from abc import abstractmethod, ABC


class Liste(ABC):
    @abstractmethod
    def insert(self, e):
        pass
    @abstractmethod
    def remove(self):
        pass
    @abstractmethod
    def find(self, e):
        pass
    @abstractmethod
    def size(self):
        pass
    @abstractmethod
    def __str__(self) :
        pass

class LineareListe(Liste):

    def __init__(self):
        self.s = 0
        self.list = []
        pass

    def appendLast(self, e):
        self.insert(e)

    def insert(self, e):
        self.list.append(e)
        self.s += 1

    def remove(self):
        e = self.list[self.s - 1]
        self.s -= 1

    def find(self, e):
        for k in range(0, self.size()):
            if self.list[k] == e:
                return k
        return -1

    def size(self):
        return self.s

    def __str__(self) :
        str1 = ""
        for e in self.list:
            str1 += str(e) + ", "
        return "[" + str1 + "]"


class Main:
    list = LineareListe()
    print("Leere Liste = " + str(list))
    list.insert(56)
    list.insert(4)
    list.insert(62)
    print("Liste mit " + str(list.size()) + " Elementen = " + str(list))
    print("Wert 56 liegt an Position " + str(list.find(56)))
    print("Wert 4 liegt an Position " + str(list.find(4)))
    print("Wert 5 liegt an Position " + str(list.find(5)))

    #Leere Liste = []
    #Liste mit 3 Elementen = [56 , 4 , 62]
    #Wert 56 liegt an Position 0
    #Wert 4 liegt an Position 1
    #Wert 5 liegt an Position -1

    ANZAHL = 10000
    MAX = ANZAHL * 100
    print("Erzeuge " + str(ANZAHL) + " zufällige Werte und füge sie ein: ")
    i = ANZAHL
    startTime = time.time()
    while i:
        list.appendLast(random.randrange(MAX))
        i -= 1
    stopTime = time.time()
    print("Das dauerte " + str((stopTime - startTime)) + "Sekunden")
    print("Finde " + str(ANZAHL) + " zufällige Werte:")
    i = ANZAHL
    treffer = 0
    misses = 0
    startTime = time.time()
    while i:
        if -1 == list.find(random.randrange(MAX)):
            misses += 1
        else:
            treffer += 1
        i -= 1
    stopTime = time.time()
    print("Das dauerte " + str((stopTime - startTime)) + "Sekunden, " + str(treffer) + "Treffer und " + str(misses) + " Misses")


class ListElement:
    def __init__(self, i, v):
        self.__id = i
        self.__value = v
    def id(self):
        return self.__id
    def value(self):
        return self.__value



