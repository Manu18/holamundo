class SchlangeAlsDVL:
    def __init__(self):
        self.__first = None
        self.__size = 0

    class ListItem():
        def __init__(self, value):
            self.value = value
            self.next = None
        def __str__(self):
            return str(self.value)

    def size(self):
        return self.__size

    def append(self, element):
        self.__size += 1
        item = self.ListItem(element)
        tmp = self.__first
        if(tmp == None):
            self.__first = item
        else:
            while(tmp.next != None):
                tmp = tmp.next
            tmp.next = item

    def remove(self):
        if(self.__size == 0):
            return None
        elif(self.__size == 1):
            self.__size -= 1
            ret = self.__first
            self.__first = None
            return ret.value
        else:
            self.__size -= 1
            tmp = self.__first
            while(tmp.next.next != None):
                tmp = tmp.next
            ret = tmp.next
            tmp.next = None
            return ret.value

    class MyIterator():
        def __init__(self, head):
            self.iterHead = head
        def __next__(self):
            if (self.iterHead == None):
                raise StopIteration
            else:
                v = self.iterHead.value
                self.iterHead = self.iterHead.next
                return v

    def __iter__(self):
        return self.MyIterator(self.__first)

    def __str__(self):
        ret = "["
        for item in self:
            ret += str(item) + ", "
        ret += "\b\b]"
        return ret

if __name__ == "__main__":
    l = SchlangeAlsDVL()
    l.append(1)
    l.append(2)
    l.append("Man on the Moon")
    print(str(l) + " has size " + str(l.size()))
    x = l.remove()
    print("x="+str ( x))
    print(str(l) + " has size " + str(l.size()))
    for i in l:
        print(i)