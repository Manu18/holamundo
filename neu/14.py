from abc import abstractmethod

def reverse(s):
    return s[::-1]
reverse("Hallo Welt!")


def euler():
    list = []
    sum = 0
    for i in range(0,1000):
        if i % 3 == 0 or i % 5 == 0:
            list.append(i)
    for e in list:
        sum += e
    print(sum)
euler()


def fib(n):
    if n < 2:
        return n
    else:
        return fib(n-1) + fib(n-2)

def fibList(n):
    list = [1,2]
    erg = [2]
    for i in range (0,n+1):
        k = list[0] + list[1]
        list[0] = list[1]
        list[1] = k
        if k > 4000000:
            break
        if k % 2 == 0:
            erg.append(k)
    return erg

def euler2():
    list = fibList(11111)
    sum = 0
    for e in list:
        if e % 2 == 0:
            sum += e
    print("Summe beträgt", sum)
euler2()

import numpy as np
weight_kg = [81.65 , 97.52 , 95.25 , 92.98 , 86.18 , 88.45]
np_weight_kg = np.array(weight_kg)
print(np_weight_kg)

lbsPerKg = 2.2
np_weight_lbs = np_weight_kg * lbsPerKg
print(np_weight_lbs)

dic = {" country ": [" Brazil ", " Russia ", " India ", " China ",
" South Africa "] ,
" capital ": [" Brasilia ", " Moscow ", " New Dehli ",
" Beijing ", " Pretoria "],
" area ": [8.516 , 17.10 , 3.286 , 9.597 , 1.221] ,
" population ": [200.4 , 143.5 , 1252 , 1357 , 52.98] }

import pandas as pd
brics = pd.DataFrame(dic)
brics.index = ["BR", "RU", "IN", "CH", "SA"]
print(brics[" population "]["BR"],"\n")

class GroupOfFour:
    size = 0
    ersterPlatz = None
    zweiterPlatz = None
    dritterPlatz = None
    vierterPlatz = None

    def __init__(self):
        self.size = 0

    @abstractmethod
    def size(self):
        pass

    @abstractmethod
    def isEmpty(self):
        pass

    @abstractmethod
    def appendLast(self, g):
        pass

    @abstractmethod
    def removeLast(self):
        pass

    @abstractmethod
    def get(self, position):
        pass

    @abstractmethod
    def swap(self, i1, i2):
        pass

    @abstractmethod
    def clear(self):
        pass

    @abstractmethod
    def __str__(self):
        pass