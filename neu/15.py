# global and local variables :
a = 1
b = 1

def func1():
    global a
    a = 2
    b = 2
    print("in func1: a=" + str(a) + ", b=" + str(b))

print("before func1: a=" + str(a) + ", b=" + str(b))
func1()
print("after func1: a=" + str(a) + ", b=" + str(b))

def prim(n):
    sieb = []
    for i in range(2,n+1):
        sieb.append(i)
    i = 0
    p = sieb[i]
    while p*p <= n:
        v = 2*p
        while v <= sieb[-1]:
            if v in sieb:
                sieb.remove(v)
            v = v+p
        i = i+1
        p = sieb[i]
    return [1] + sieb

def largestPrimeFactor(n):
    i = 2
    while (i * i < n):
        while (n % i == 0):
            n /= i
        i += 1
    return int(n)
print(largestPrimeFactor(600851475143))