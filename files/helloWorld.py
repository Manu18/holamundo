print("Hello World")
name = "Manuel"
alter = 21
jahr = 2020
if name == "Manuel":
    print("Hallo " + name)
elif name == "Fischer":
    print("Guten Tag Herr " + name)
elif len(name) > 10:
    print("Name länger als 10 Buchstaben")
else:
    print("Falscher Name")
    
gebjahr = jahr - alter
i = 1
while i <= alter:
    print(i, ". Geburtstag: ", gebjahr)
    gebjahr = gebjahr + 1
    i = i + 1
 
liste = ['Java', 'Python', 'C', 'SQL', 'JavaScript', 'PHP']
liste2 = [21,5443,123,654,12,523,12,43,12,12,531,435423,234]
for worte in liste:
    print(worte)
    
for k in range(1,17,2):
    print(k)
    if k >= len(liste2):
        break
    if (k == 1):
        continue
    print(liste2[k])
    
    
def reihen(n,bis):
    for j in range(0,bis + 1):
        print(j*n)
    print("Funktion zu ende")
    
print("Funktionsaufruf")
reihen(3,30)

def fib(n):
    if n == 0:
        return 0
    if n == 1:
        return 1
    else:
        return fib(n-1) + fib(n-2);

def fibreihe(m=10):
    for n in range(0,m+1):
        print(fib(n))

print(fib(10))
fibreihe()

def listAppend(L=None):
    if L is None:
        L = []
    L.append(42)
    return L

liste3 = [1,2,3]
print(listAppend(liste3))

import testmodul
testmodul.testModule()

 # Modul sys wird importiert:
import sys

# Iteration über sämtliche Argumente:
for eachArg in sys.argv:
        print eachArg

if __name__ == "__main__":
    print("Hello World in Main!!!")
    print("Diese funktion wird nur beim direkten Aufruf der Datei ausgeführt, aber nicht bei einem import.")
   
    in1 = input("Alter Eingabe: ")
    print("Alter in 10 Jahren " + str(int(in1) + 10))
    print(type(in1))
    
    f = open('text.txt', 'r+')
    print(f.read(3))
    print(f.readline())
    for line in f:
        print(line)
    f.write("\n3. Zeile")
    f.read()
    f.close()
    f2 = open('text2.txt', 'w')
    f2.write("Neue Datei")
    f2.close()
    global gvar
    gvar = "Ich bin global"
    # nonlocal nvar
    # nvar = "Ich bin nicht lokal"
    



    
