
    
def getTemperature():
    while True:
        c = input("Geben Sie die Temperatur in Celsius ein: ")
        try:
            c = float(c)
            return c
        except ValueError:
            print("Das ist keine akzeptierte Zahl")

def convertCtoK(c):
    k = c + 273.15
    return k
    
if __name__ == "__main__":
    print("Willkommen beim Temperatur Rechner!")
    c = getTemperature()
    k = convertCtoK(c)
    print(c, "Grad Celsius sind ", k, " Kelvin") 