from abc import abstractmethod


class AListe:
	@abstractmethod
	def size(self):
		pass

	@abstractmethod
	def append(self, element):
		pass

	@abstractmethod
	def remove(self):
		pass


class SchlangeAlsDVL(AListe):
	def __init__(self):
		self.len = 0
		self.first = None
		self.last = None

	def __iter__(self):
		return self.MyIterator(self.first)

	class MyIterator:
		def __init__(self, head):
			self.iterHead = head

		def __next__(self):
			if self.iterHead is None:
				raise StopIteration
			else:
				v = self.iterHead.item
				self.iterHead = self.iterHead.next
				return v

	def __str__(self):
		listS = "["
		for i in self:
			listS += str(i) + ","
		listS = listS[0:len(listS) - 1] + "]"
		return listS

	class ListItem:
		def __init__(self, data):
			self.item = data
			self.next = None
			self.prev = None

		def __str__(self):
			return "" + self.item

	def size(self):
		return self.len

	def append(self, data):
		element = self.ListItem(data)
		if self.first is None:
			self.first = element
			self.last = element
		element.prev = self.last
		self.last.next = element
		self.last = element
		self.len += 1

	def remove(self):
		item = self.last
		if self.len != 0:
			self.last = self.last.prev
			self.last.next = None
			self.len -= 1
			return item
		else:
			return None


l = SchlangeAlsDVL()
l.append(1)
l.append(2)
l.append("Man on the Moon")
print(str(l) + " has size " + str(l.size()))

x = l.remove()
print("x=" + str(x))
print(str(l) + " has size " + str(l.size()))

for i in l:
	print(i)
