# global and local variables
a = 1
b = 1 

def func1():
    global a
    a = 2
    b = 2
    print("in func1: a="+str(a)+", b="+str(b))
	
print("before func1: a="+str(a)+", b="+str(b)) 
func1() 
print("after func1: a="+str(a)+", b="+str(b))


